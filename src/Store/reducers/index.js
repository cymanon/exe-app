import * as actionType from '../../constants/ActionTypes'

const initialState = {
    exercises: [
        {
            category: 'Abs',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            id: 0,
            name: 'Crunches'
        },
        {
            category: 'Legs',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            id: 1,
            name: 'Sumo squat'
        },
        {
            category: 'Arms',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            id: 2,
            name: 'Biceps curl'
        },
        {
            category: 'Cardio',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            id: 3,
            name: 'Burpees'
        },
    ],
    programs: [
        {
            id: '123214dsadasjhwqah',
            name: 'Test Program',
            description: 'cardio, srardio',
            segments: [
                {
                    numberOfExercises: 3,
                    exerciseTime: 33,
                    breakTime: 3,
                    exercises: ['biceps curl', 'sumo squat', 'burpee'],
                }
            ]
        },
        {
            id: '123214dsadasjhwqa12312h',
            name: 'Test Program 2',
            description: 'siła, masa',
            segments: [
                {
                    numberOfExercises: 3,
                    exerciseTime: 33,
                    breakTime: 3,
                    exercises: ['biceps curl', 'sumo squat', 'burpee'],
                }
            ]
        }
    ]
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
    case actionType.ADD_EXERCISE:

        return {
            ...state,
            exercises: [...state.exercises, action.newExercise]
        }

    case actionType.DELETE_EXERCISE:

        return {
            ...state,
            exercises: [...state.exercises].filter(program => program.id !== action.exerciseId)
        }   
        
    case actionType.EDIT_EXERCISE:

        return {
            ...state,
            exercises: state.exercises.map(ex => ex.id === action.exerciseToEdit.id ? { ...action.exerciseToEdit } : ex)
        }

    case actionType.ADD_PROGRAM:
        
        return {
            ...state,
            programs: [...state.programs, action.newProgram]
        }

    case actionType.EDIT_PROGRAM:

        return {
            ...state,
            programs: state.programs.map(pr => pr.id === action.programToEdit.id ? {...action.programToEdit} : pr)
        }

    default:
        return state
    }
}

export default rootReducer

// Remember two key points for avoiding mutations in Redux:
// use concat, slice, or the spread operator for arrays
// use Object.assign or object spread of objects