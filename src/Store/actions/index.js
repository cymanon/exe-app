import * as actions from '../../constants/ActionTypes'

export const addExercise = newExercise => {
    return { type: actions.ADD_EXERCISE, newExercise }
}

export const deleteExercise = exerciseId => {
    return { type: actions.DELETE_EXERCISE, exerciseId }
}

export const editExercise = exerciseToEdit => {
    return { type: actions.EDIT_EXERCISE, exerciseToEdit }
}

export const addProgram = newProgram => {
    return { type: actions.ADD_PROGRAM, newProgram }
}

export const editProgram = programToEdit => {
    return { type: actions.EDIT_PROGRAM, programToEdit }
}

