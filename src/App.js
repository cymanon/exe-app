import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

import './App.css'
import ExercisesContainer from './components/exercises/ExercisesContainer'
import ProgramsContainer from './components/programs/ProgramsContainer'

function App() {
    return (
        <Router>
            <div>
                <nav className='navigation'>
                    <ul>
                        <li><Link to='/exercises'>Exercises</Link></li>
                        <li><Link to='/programs'>Programs</Link></li>
                    </ul>
                </nav>
                <Switch>
                    <Route path='/exercises'><ExercisesContainer /></Route>
                    <Route path='/programs'><ProgramsContainer /></Route>
                </Switch>
            </div>
        </Router>
    
    )
}

export default App