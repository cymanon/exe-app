import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types'

import Input from '../UI/Input'
import Button from '../UI/Button'

import { editExercise, addExercise } from '../../store/actions/index'
import * as exercisesCategories from '../../constants/Categories'

const ExerciseForm = ({ id = null, nameVal = '', descVal = '', catVal = '', type, onClose}) => {
    const dispatch = useDispatch()
    
    const [name, setName] = useState(nameVal)
    const [description, setDescription] = useState(descVal)
    const [category, setCategory] = useState(catVal)

    const onSubmitHandler = event => {
        event.preventDefault()
        
        const exerciseId = id ?? Math.floor(new Date().getTime() * Math.random())

        const exercise = {
            id: exerciseId,
            name: name,
            description: description,
            category: category
        }

        if (type === 'add') {
            dispatch(addExercise(exercise))
            setName('')
            setDescription('')
        } else if (type === 'edit') {
            dispatch(editExercise(exercise))
        }
        event.target.reset()
        onClose()
    }

    const form = <form onSubmit={onSubmitHandler}>
        <Input
            type='text'
            label='Exercise name'
            value={name}
            func={(event) => setName(event.target.value)}
        />
        <Input
            type='textarea'
            label='Description'
            value={description}
            func={(event) => setDescription(event.target.value)}
        />
        <Input
            type='select'
            selectedValue={category}
            label='Category'
            value={category}
            func={(event) => setCategory(event.target.value)}
            options={Object.values(exercisesCategories)}
        />
        <Button
            type='submit'
            value='Submit'
            disabled={!name || !description || !category}>Save exercise</Button>
        {type === 'add' ? <Button onClick={onClose}>Cancel</Button> : ''}
    </form>

    return form
}

ExerciseForm.propTypes = {
    id: PropTypes.number,
    nameVal: PropTypes.string,
    descVal: PropTypes.string,
    catVal: PropTypes.string,
    type: PropTypes.string,
    onClose: PropTypes.func
}

export default ExerciseForm