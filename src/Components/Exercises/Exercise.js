import React, { useState } from 'react'
import * as messages from '../../constants/ErrorMessages'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'

import styles from './Exercise.module.css'

import Button from '../UI/Button'
import Modal from '../UI/Modal'

import EditExercise from './EditExercise'
import { deleteExercise } from '../../store/actions/index' 

const Exercise = props => {
    const dispatch = useDispatch()

    const [editModalVisible, setEditModalVisible] = useState(false)
    
    const style = styles[`${props.category}`]
    const exerciseDisplay = (
        <div key={props.id} className={styles.exercise}>
            <h3>{props.name}</h3>
            <p>{props.description}</p>
            <p className={style}>{props.category}</p>
            <Button onClick={() => dispatch(deleteExercise(props.id))}>Delete</Button>
            <Button onClick={() => setEditModalVisible(true)}>Edit</Button>
        </div>
    )

    const editedExerciseModal = editModalVisible ? <Modal onClick={() => setEditModalVisible(false)} ><EditExercise onSave={() => setEditModalVisible(false)} details={props}/></Modal> : null

    return (
        <>
            {exerciseDisplay ? exerciseDisplay : <p>{messages.NO_EXERCISES}</p>}
            {editedExerciseModal}
        </>
    )
}

Exercise.propTypes = {
    category: PropTypes.string,
    name: PropTypes.string,
    id: PropTypes.number,
    description: PropTypes.string,
}

export default Exercise