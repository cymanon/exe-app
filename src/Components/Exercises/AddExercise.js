import React, { useState } from 'react'

import styles from './AddExercise.module.css'

import ExerciseForm from './ExerciseForm'
import Button from '../UI/Button'

const AddExercise = () => {
    const [showForm, setShowForm] = useState(false)
    const toggleFormVisibilityHandler = () => setShowForm(prevState => !prevState)

    let form = showForm ? 
        <ExerciseForm type='add' onClose={toggleFormVisibilityHandler}/> :
        <Button onClick={toggleFormVisibilityHandler}>Add exercise</Button>

    return (
        <div className={styles['add-exercise']}>           
            <div className='container'>
                {form}                
            </div>
        </div>
    )
    
}

export default AddExercise
