import React from 'react'
import AddExercise from './AddExercise'
import ExercisesList from './ExercisesList'

const ExercisesContainer = () => {

    return (
        <div>
            <AddExercise />
            <ExercisesList />
        </div>
    )
}

export default ExercisesContainer