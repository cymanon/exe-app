import React from 'react'
import PropTypes from 'prop-types'

import ExerciseForm from './ExerciseForm'

const EditExercise = ({ details, onSave}) => {
    const { id, name, description, category } = details
    
    const form = <ExerciseForm 
        id={id}
        nameVal={name}
        descVal={description}
        catVal={category}
        type='edit'
        onClose={onSave}/>

    return name && description && category ? form : <p>Loading</p>
}

EditExercise.propTypes = {
    details: PropTypes.object,
    onSave: PropTypes.func,
}

export default EditExercise