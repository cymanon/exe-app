import React from 'react'
import { useSelector } from 'react-redux'

import Exercise from './Exercise'

const ExercisesList = () => {
    const exercises = useSelector(state => state.exercises)

    return exercises.map(exercise => <Exercise
        key={exercise.id}
        id={exercise.id}
        name={exercise.name}
        description={exercise.description}
        category={exercise.category}
    />)
}

export default ExercisesList