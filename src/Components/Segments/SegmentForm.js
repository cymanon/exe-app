import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'

import Input from '../UI/Input'
import Button from '../UI/Button'

const SegmentForm = ({ numberOfExercises = 1, breakTime = 1, exerciseTime = 1, exercises = [], onSave, onClose}) => {
    const exercisesOptions = useSelector(state => state.exercises)

    const [exQty, setExQty] = useState(numberOfExercises)
    const [exTime, setExTime] = useState(exerciseTime)
    const [brTime, setBrTime] = useState(breakTime)
    const [chosenExercises, setChosenExercises] = useState(exercises)

    useEffect(() => {
        if (chosenExercises.length === exQty) {
            return
        } else if (chosenExercises.length < exQty) {
            setChosenExercises(prevState => [...prevState, ...new Array(exQty - prevState.length)])
        } else {
            setChosenExercises(prevState => prevState.slice(0, exQty))
        }
    }, [exQty])

    const chooseExerciseHandler = (value, id) => {
        setChosenExercises(prevState => {
            const previous = [...prevState]
            previous[id] = value

            return previous
        })
    }

    const saveSegmentHandler = event => {
        event.preventDefault()

        const newSegment = {
            numberOfExercises: exQty,
            exerciseTime: exTime,
            breakTime: brTime,
            exercises: chosenExercises,
        }

        onSave(newSegment)
        onClose()
    }

    const Inputs = () => {
        let inputs = []
        for (let i = 0; i < exQty; i++) {
            inputs.push(
                <Input
                    key={i}
                    type='select'
                    label=''
                    selectedValue={chosenExercises[i]}
                    func={(event) => chooseExerciseHandler(event.target.value, i)}
                    options={exercisesOptions.map(ex => ex.name)}
                />)
        }
        return inputs
    }

    return (
        <div>
            <div style={{display: 'inline-flex'}}>
                <Input
                    type='number'
                    label='Number of exercises:'
                    placeholder='quantity'
                    value={exQty}
                    func={(event) => setExQty(+event.target.value)}
                />

                <Input
                    type='number'
                    label='Exercise time:'
                    placeholder='in seconds'
                    value={exTime}
                    func={(event) => setExTime(+event.target.value)}
                />

                <Input
                    type='number'
                    label='Break time:'
                    placeholder='in seconds'
                    value={brTime}
                    func={(event) => setBrTime(+event.target.value)}
                />
            </div>
            <Inputs />
            <Button
                type='submit'
                value='Submit'
                disabled={!exQty || !exTime || chosenExercises.some(ex => !ex)}
                onClick={saveSegmentHandler}>Add Segment</Button>
        </div>
    )
}


SegmentForm.propTypes = {
    numberOfExercises: PropTypes.number,
    breakTime: PropTypes.number,
    exerciseTime: PropTypes.number,
    exercises: PropTypes.array,
    onSave: PropTypes.func,
    onClose: PropTypes.func,
}

export default SegmentForm