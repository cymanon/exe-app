import React, { useState } from 'react'
import styles from './SegmentsList.module.css'
import PropTypes from 'prop-types'

import SegmentForm from './SegmentForm'
import EditIcon from '../UI/EditIcon'
import Modal from '../UI/Modal'

const SegmentsList = ({ segments, onEdit }) => {
    const [showEditModal, setShowEditModal] = useState(false)
    const [editedSegmentDetails, setEditedSegmentDetails] = useState(false)
    const [editedId, setEditedId] = useState(null)

    const editSegmentHandler = (segment, index) => {
        setShowEditModal(true)
        setEditedId(index)
        setEditedSegmentDetails(segment)
    }

    const saveChangesHandler = changedSegment => {
        const allSegments = [...segments]
        allSegments[editedId] = changedSegment

        onEdit(allSegments)
    }

    const segmentsDisplay = segments.map((seg, index) => {
        const totalSegmentTime = (seg.exerciseTime + seg.breakTime) * seg.exercises.length
        const seconds = totalSegmentTime % 60
        const minutes = (totalSegmentTime - seconds) / 60
        
        return (
            <div className={styles.segment} key={`Segment_${index}`}>
                <p>Exercise Time: <span className={styles.time}>{seg.exerciseTime}s</span> 
                    Break Time: <span className={styles.time}>{seg.breakTime}s</span> 
                    Total length: <span className={styles.time}>{minutes}min {seconds}s</span></p>
                <ul>{seg.exercises.map((el, id) => <li key={`Exercise_${id}`}>{el}</li>)}</ul>
                <EditIcon onClick={() => editSegmentHandler(seg, index)}/>
            </div>
        )
    })

    return (
        <div className={styles['segments-container']}>
            {showEditModal ? <Modal onClick={() => setShowEditModal(false)}>
                <SegmentForm 
                    numberOfExercises={editedSegmentDetails.numberOfExercises}
                    breakTime={editedSegmentDetails.breakTime}
                    exerciseTime={editedSegmentDetails.exerciseTime}
                    exercises={editedSegmentDetails.exercises}
                    onSave={saveChangesHandler}
                    onClose={() => setShowEditModal(false)} />
            </Modal> : null}
            {segmentsDisplay}
        </div>
    )
}

SegmentsList.propTypes = {
    segments: PropTypes.array,
    onEdit: PropTypes.func
}

export default SegmentsList