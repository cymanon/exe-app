import React from 'react'
import PropTypes from 'prop-types'

import styles from './Input.Module.css'

const Input = ({ type, label, func, value, options, selectedValue, placeholder}) => {
    let insides
    switch (type) {
    case 'text':
        insides = <input type="text" onChange={func} value={value} placeholder={placeholder}/>
        break
    case 'number':
        insides = <input type="number" onChange={func} value={value} placeholder={placeholder}/>
        break
    case 'textarea':
        insides = <textarea value={value} onChange={func} placeholder={placeholder}/>
        break
    case 'select': {
        let firstOption = selectedValue ? null : <option />
        insides = <select value={selectedValue} onChange={func} placeholder={placeholder}>
            {firstOption}
            {options.map((opt, index) => {
                return (<option value={opt} key={index}>{opt.toUpperCase()}</option>)
            })}</select>
        break
    }
    
    default:
        break
    }

    insides = label ? <label>{label} {insides}</label> : insides

    return <div className={styles.container}><p className={styles.inputs}>{insides}</p></div>
}

Input.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string,
    selectedValue: PropTypes.string,
    placeholder: PropTypes.string,
    func: PropTypes.func,
    value: PropTypes.any,
    options: PropTypes.array,
}


export default Input