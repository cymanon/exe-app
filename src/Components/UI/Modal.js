import React from 'react'
import PropTypes from 'prop-types'

import styles from './Modal.Module.css'

import Button from './Button'

const ErrorModal = props => {
    return (
        <div className={styles.backdrop}>
            <div className={styles.modal}>
                <h3>{props.message}</h3>
                <div>{props.children}</div>
                <Button onClick={props.onClick}>Close</Button>
            </div>
        </div>
    )
}

ErrorModal.propTypes = {
    message: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.object
}

export default ErrorModal