import React from 'react'
import styles from './EditIcon.module.css'
import PropTypes from 'prop-types'

const EditIcon = ({onClick}) => (
    <div className={styles.icon} onClick={onClick}>
        <div></div>
        <div></div>
    </div>
)

EditIcon.propTypes = {
    onClick: PropTypes.func
}


export default EditIcon