import React from 'react'
import PropTypes from 'prop-types'

import styles from './Button.module.css'

const Button = ({type, onClick, disabled, children}) => {
    const style = disabled ? `${styles.button} ${styles.invalid}` : styles.button

    return <button
        type={type}
        className={style}
        disabled={disabled}
        onClick={onClick}>{children}
    </button>
}

Button.propTypes = {
    type: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    children: PropTypes.string,
}


export default Button