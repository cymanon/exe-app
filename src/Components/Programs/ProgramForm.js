import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types'

import Input from '../UI/Input'
import Button from '../UI/Button'

import SegmentForm from '../segments/SegmentForm'
import SegmentsList from '../segments/SegmentsList'

import * as workoutSegments from '../../constants/WorkoutSegments'
import { addProgram, editProgram } from '../../store/actions/index'

const ProgramForm = ({ id = null, programName = '', programDesc = '', segments = [], onCancel, type }) => {
    const dispatch = useDispatch()

    const [name, setName] = useState(programName)
    const [description, setDescription] = useState(programDesc)
    const [allSegments, setAllSegments] = useState(segments)

    const [segmentType, setSegmentType] = useState('')
    const [showSegments, setShowSegments] = useState(false)
    const [formIsValid, setFormIsValid] = useState(false)

    useEffect(() => setShowSegments(true), [segmentType])
    useEffect(() => {
        if (name && allSegments.length) {
            setFormIsValid(true)
        }
    }, [name, allSegments])


    const saveProgramHandler = e => {
        e.preventDefault()
        const programId = id ?? `PROGRAM_${Math.floor(new Date().getTime() * Math.random())}`
        
        const program = {
            id: programId,
            name,
            description,
            segments: allSegments
        }

        if (type === 'add') {
            dispatch(addProgram(program))
        } else if (type === 'edit') {
            dispatch(editProgram(program))
        }
        onCancel()
    }

    const onCancelSegmentHandler = () => setSegmentType('')
    const onAddSegmentHandler = newSegment => setAllSegments(prevState => [...prevState, newSegment])
    const onEditSegmentHandler = allSegments => setAllSegments(allSegments)

    const segmentForm = !segmentType ? null : showSegments ? <SegmentForm onSave={onAddSegmentHandler} onClose={onCancelSegmentHandler} /> : null

    return (
        <div>
            <h1>{`${type === 'add' ? 'Add' : 'Edit'} Program`}</h1>
            <form onSubmit={saveProgramHandler}>
                <Input
                    type='text'
                    label='Name'
                    placeholder='Name it'
                    value={name}
                    func={(event) => setName(event.target.value)}
                />
                <Input
                    type='text'
                    label='Description'
                    placeholder='Optional description'
                    value={description}
                    func={(event) => setDescription(event.target.value)}
                />
                <SegmentsList onEdit={onEditSegmentHandler} segments={allSegments} />
                <Input
                    selectedValue={segmentType}
                    type='select'
                    label='Pick segment type:'
                    value={segmentType}
                    func={(event) => setSegmentType(event.target.value)}
                    options={Object.values(workoutSegments)}
                />
                {segmentForm}
                <Button
                    type='submit'
                    value='Submit'
                    disabled={!formIsValid}>Submit Program</Button>
            </form>
        </div>
    )
}

ProgramForm.propTypes = {
    segments: PropTypes.array,
    programName: PropTypes.string,
    programDesc: PropTypes.string,
    type: PropTypes.string,
    id: PropTypes.string,
    onCancel: PropTypes.func
}

export default ProgramForm