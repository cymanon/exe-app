import React, { useState } from 'react'

import AddProgram from './ProgramForm'
import ProgramsList from './ProgramsList'

import Button from '../UI/Button'

const ProgramsContainer = () => {
    const [showProgramForm, setShowProgramForm] = useState(false)

    let addProgramPanel = showProgramForm ?
        <AddProgram onCancel={() => setShowProgramForm(false)} type='add'/> :
        <Button onClick={() => setShowProgramForm(true)}>Add Program</Button>
    
    return (
        <div>
            {addProgramPanel}
            <ProgramsList />
        </div>
    )
}

export default ProgramsContainer