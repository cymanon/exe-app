import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import styles from './ProgramsList.module.css'

import ProgramForm from './ProgramForm'
import EditIcon from '../UI/EditIcon'
import Modal from '../UI/Modal'

const ProgramsList = () => {
    const programs = useSelector(state => state.programs)

    const [showEditModal, setShowEditModal] = useState(false)
    const [editedProgramDetails, setEditedProgramDetails] = useState(false)

    const editProgramHandler = program => {
        setShowEditModal(true)
        setEditedProgramDetails(program)
    }

    const programsDisplay = programs.map((pr, index) => {
        const totalTime = pr.segments.reduce((res, seg) => res + (seg.exerciseTime + seg.breakTime) * seg.numberOfExercises, 0)
        const seconds = totalTime % 60
        const minutes = (totalTime - seconds) / 60
        
        return (
            <div className={styles.program} key={index}>
                <div className={styles.details}>
                    <h2>{pr.name}</h2>
                    <p>Duration time: {`${minutes}min ${seconds}s`}</p>
                    <EditIcon onClick={() => editProgramHandler(pr)} />
                    <p>{pr.description}</p>
                </div>
            </div>
        )
    })

    return (
        <div className={styles['main-container']}>
            {showEditModal ? <Modal onClick={() => setShowEditModal(false)}>
                <ProgramForm
                    programName={editedProgramDetails.name}
                    segments={editedProgramDetails.segments}
                    id={editedProgramDetails.id}
                    programDesc={editedProgramDetails.description}
                    type='edit'
                    onCancel={() => setShowEditModal(false)}/></Modal> : null}
            {programsDisplay}
        </div>
    )
}

export default ProgramsList