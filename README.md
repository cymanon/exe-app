# Exercise Planner
Simple app to create and save your workout plans.

## Created with:

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Scripts: 
After installing npm, run: 

`npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.